﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowNotificationService
{
    class NotificationService
    {

        public dynamic SendNotification(int tripId,int claimId,string message)
        {
            var url = ConfigurationManager.AppSettings["TraveoApi"]+"Email/";
            var client = new RestClient(url);

            var request = new RestRequest("SendNotificationEmail", Method.GET);
            request.AddParameter("tripId", tripId);
            request.AddParameter("claimId", claimId);
            request.AddParameter("reminderAddition", message);

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            Console.WriteLine("trip approver service response received with content-" + content);
            return content;
        }
    }
}
