﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MailResponderService.models;
using System.Configuration;

namespace MailResponderService.Activities
{
    public static class SqlHelper
    {
        public static int getLastApproverByClaimId(int claimId)
        {
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            var query = String.Format("select top 1 approverid from claim.claimapproverlist where claimid={0} order by claimapproverlistid desc ", claimId);
            int id = 0;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {

                            id = Convert.ToInt32(sqlDataReader["approverid"].ToString());
                        }
                    }
                }
            }
            return id;
        }


        public static int getLastApproverByTripId(int tripId)
        {
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            var query = String.Format("select top 1 approverid from trip.tripapproverlist where tripid={0} order by tripapproverlistid desc ", tripId);
            int id = 0;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {

                            id = Convert.ToInt32(sqlDataReader["approverid"].ToString());
                        }
                    }
                }
            }
            return id;
        }

        public static int GetClaimIdByTripId(string tripId)
        {
            string conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            string query = string.Format("select ClaimDetailId from claim.claimdetail where tripid={0}", tripId);
            int id = 0;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            id = Convert.ToInt32(sqlDataReader["ClaimDetailId"]);
                        }
                    }
                }
            }
            return id;
        }
        public static int GetIdByEmail(string email)
        {
            string conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            string query = string.Format("select EmployeeId from employee.employee where SecondaryEmail='{0}'", email);
            int id = 0;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            id = Convert.ToInt32(sqlDataReader["EmployeeId"]);
                        }
                    }
                }
            }
            return id;
        }

        public static NotificationEmail GetNotificationByTripId(int tripId)
        {
            string query = string.Format("SELECT TD.TripType,TD.EmployeeId,TD.DoneById,TD.CompanyId,TD.TripDescription,TD.StartDate,E.FirstName employeeName,E.FirstName + ' ' + ISNULL(E.MiddleName,' ') + ' '+ E.LastName+'(EmployeeNumber-'+E.employeenumber+')'  AS EmpName, 'from '+  CDO.CityName + ' to '+(SELECT CASE WHEN TD.TripType=1016 THEN C.CityName ELSE (SELECT C.CityName + ' ( Round Trip ) ' FROM Trip.TripDestination D INNER JOIN [ADMIN].[CityNew] C ON C.CityId=D.DestinationCityId WHERE TripId=T.TripId AND D.StayHereTill=D.ReachHereby) END FROM TRIP.TripDestination TTDD INNER JOIN [ADMIN].[CityNew] C ON C.CityId=TTDD.DestinationCityId WHERE TripDestinationId=MAX(TDD.TripDestinationId)) +' on ' AS City, CONVERT(VARCHAR(15), cast(TD.StartDate AS DATE),106) + ' - '+ (SELECT CASE WHEN TD.TripType=1016 THEN CONVERT(VARCHAR(15), cast(StayHereTill AS DATE),106) ELSE (SELECT CONVERT(VARCHAR(15), cast(ReachHereby AS DATE),106) FROM Trip.TripDestination D WHERE TripId=T.TripId AND D.StayHereTill=D.ReachHereby) END FROM TRIP.TripDestination WHERE TripDestinationId=MAX(TDD.TripDestinationId)) +' is pending approval' AS ExpenseDate ,T.[IsViewed],T.TripId,T.ModifiedDate ,2 AS ClaimorTripType, 0 as IsEmployeeNotification," +
                "EMP.secondaryemail,EMP.FirstName,C.name FROM Trip.TripApproverList T INNER JOIN Trip.TripDetail TD ON TD.TripId=T.TripId INNER JOIN Trip.TripDestination TDD ON TDD.TripId=TD.TripId INNER JOIN [ADMIN].[CityNew] CDO ON CDO.CityId=TD.TripOriginId INNER JOIN Employee.Employee E ON E.EmployeeId=TD.EmployeeId INNER JOIN Employee.Employee EMP ON T.ApproverId=EMP.EmployeeID " +
                "Inner Join Company.company C on C.companyId=TD.companyId " +
                "where T.tripid={0} and T.statusid=1000 " +
                "GROUP BY C.Name,E.employeenumber,TD.EmployeeId,TD.DoneById,TD.CompanyId,E.FirstName,E.LastName,E.MiddleName,CDO.CityName,TD.StartDate,TD.TripType,T.IsViewed,T.TripId,T.[ModifiedDate],EMP.secondaryemail,EMP.FirstName,TD.TripDescription order by T.ModifiedDate DESC", tripId);
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            NotificationEmail email = new NotificationEmail();
            email.subject = "Traveo: Travel approval requested by ";
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            email.approverEmail = reader["secondaryemail"].ToString();
                            email.approverName = reader["FirstName"].ToString();
                            email.employeeId = Convert.ToInt32(reader["EmployeeId"]);
                            email.doneById = Convert.ToInt32(reader["DoneById"]);
                            email.companyId = Convert.ToInt32(reader["CompanyId"]);
                            email.content = reader["EmpName"].ToString() + " has submitted a travel request which requires your approval. The details of the trip are given below. You can reply to this email with 'Yes' or 'No' to approve or reject this request. Alternatively, you can also login to ";
                            DateTime d = new DateTime();
                            DateTime.TryParse(reader["StartDate"].ToString(), out d);
                            email.companyName = reader["name"].ToString();
                            email.tripType = Convert.ToInt32(reader["TripType"]);
                            email.subject += reader["employeeName"].ToString() + " Travel Date-" + d.ToString("dd MMM yyyy");
                        }
                    }
                }
            }
            return email;
        }
        public static NotificationEmail GetNotificationByClaimId(int claimId)
        {
            string query = string.Format("select emp2.firstname employeeName,cd.expensesreportname,emp2.FirstName + ' ' + ISNULL(emp2.MiddleName,' ') + ' '+ emp2.LastName +'(EmployeeNumber-'+emp2.employeenumber +') has submitted a claim request which requires your approval <br> You can reply to this email with ''Yes'' or ''No'' to approve or reject this request. Alternatively, you can also login to  ' as content ,emp1.PrimaryEmail,emp1.FirstName,C.name " +
               "from claim.claimapproverlist cal inner join claim.claimdetail cd on cd.claimdetailid=cal.claimid inner join employee.employee emp1 on cal.approverid=emp1.employeeid inner join employee.employee emp2 on cal.employeeid=emp2.employeeid inner join Company.company C on C.companyId=cd.CompanyId " +
               " where ((cal.statusid=1002 and cal.approverroleid!=1) or (cal.statusid=1003 and cal.approverroleid=1)) and cal.claimid={0}", claimId);
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            NotificationEmail email = new NotificationEmail();
            email.subject = "Traveo: Claim approval requested by";
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            email.approverEmail = reader["PrimaryEmail"].ToString();
                            email.approverName = reader["FirstName"].ToString();
                            email.content = reader["content"].ToString();
                            email.companyName = reader["name"].ToString();
                            email.subject += reader["employeeName"].ToString() + "- Claim: " + reader["expensesreportname"].ToString();
                        }
                    }
                }
            }
            return email;
        }

        public static bool IsEmailApprovalAllowed(int claimId, int tripId)
        {
            List<string> isallowed = new List<string>();
            string query = "";
            if (tripId != 0)
            {
                query = string.Format("select cp.preferencevalue from company.companypreference cp inner join trip.tripdetail td on td.companyid=cp.companyid where cp.preferencetype='TripEmailApprovals' and td.tripid={0}", tripId);
            }
            else
            {
                query = string.Format("select cp.preferencevalue from company.companypreference cp inner join Claim.ClaimDetail CD on CD.CompanyId=CP.CompanyId and CP.PreferenceType='ClaimEmailApprovals' where CD.ClaimDetailId={0}", claimId);
            }
            string conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            isallowed.Add(reader["preferencevalue"].ToString());
                        }
                    }
                }
            }
            if (isallowed.Contains("NotAllowed"))
                return false;
            return true;
        }
    }
}
