﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WorkflowNotificationService.Models;
using System.Configuration;

namespace WorkflowNotificationService.Activities
{
    public static class SqlHelper
    {
        public static List<ReminderLog> getBasicClaimReminderLogs()
        {
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            var query = String.Format("select CAL.ClaimApproverListId, CAL.ClaimId, CAL.ApproverId, RS.RemiderMessage, DATEDIFF(minute, Coalesce(RL.SentDateTime, CAL.insertedDate, getdate()), getdate())MinutesTillLastSent, Isnull(RS.MinutesAfterLastReminder,0)MinutesAfterLastReminder, Isnull(RL.ReminderLevel, 0)LastSentReminderLevel, Isnull(RS.ReminderLevel,0) NextReminderLevel from claim.ClaimApproverList CAL inner join claim.ClaimDetail CD on CD.ClaimDetailId = CAL.ClaimId left join Company.ReminderLog RL on RL.ApproverListId = CAL.ClaimApproverListId and RL.EmployeeId = CAL.ApproverId and RL.ClaimId = CAL.ClaimId inner join Company.ReminderSetting RS on RS.CompanyId = CD.CompanyId and RS.ReminderLevel = ISNull(RL.ReminderLevel, 0) + 1 where ClaimApproverListId = (select max(CA.ClaimApproverListId) from claim.ClaimApproverList CA where CA.ClaimId = CAL.ClaimId) and CAL.StatusId = 1002 order by CAL.ClaimId desc");
            List<ReminderLog> BasicLogs = new List<ReminderLog>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            ReminderLog r = new ReminderLog();
                            r.ApproverListId = Convert.ToInt32(sqlDataReader["ClaimApproverListId"]);
                            r.ClaimId = Convert.ToInt32(sqlDataReader["ClaimId"]);
                            r.EmployeeId = Convert.ToInt32(sqlDataReader["ApproverId"]);
                            r.LastSentReminderLevel = Convert.ToInt32(sqlDataReader["LastSentReminderLevel"]);
                            r.MinutesAfterLastReminder = Convert.ToInt32(sqlDataReader["MinutesAfterLastReminder"]);
                            r.MinutesSinceLastSent = Convert.ToInt32(sqlDataReader["MinutesTillLastSent"]);
                            r.NextReminderLevel = Convert.ToInt32(sqlDataReader["NextReminderLevel"]);
                            r.ReminderMessage = sqlDataReader["RemiderMessage"].ToString();
                            if (!string.IsNullOrEmpty(r.ReminderMessage))
                            {
                                BasicLogs.Add(r);
                            }
                        }
                    }
                }
            }
            return BasicLogs;
        }
        public static List<ReminderLog> getBasicTripReminderLogs()
        {
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            var query = String.Format("select TAL.TripApproverListId,TAL.TripId,TAL.ApproverId,RS.RemiderMessage,DATEDIFF(minute,Coalesce(RL.SentDateTime,TAL.insertedDate,getdate()),getdate())MinutesTillLastSent,Isnull(RS.MinutesAfterLastReminder,0)MinutesAfterLastReminder,Isnull(RL.ReminderLevel,0)LastSentReminderLevel,Isnull(RS.ReminderLevel,0) NextReminderLevel from Trip.TripApproverList TAL inner join Trip.TripDetail TD on TD.TripId = TAL.TripId left join Company.ReminderLog RL on RL.ApproverListId = TAL.TripApproverListId and RL.EmployeeId = TAL.ApproverId and RL.TripId = TAL.TripId inner join Company.ReminderSetting RS on RS.CompanyId = TD.CompanyId and RS.ReminderLevel = ISNull(RL.ReminderLevel, 0) + 1 where TripApproverListId = (select max(TA.TripApproverListId) from Trip.TripApproverList TA where TA.TripId = TAL.TripId)and TAL.StatusId = 1000 order by TAL.TripId desc");

            List<ReminderLog> BasicLogs = new List<ReminderLog>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    using (SqlDataReader sqlDataReader = cmd.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            ReminderLog r = new ReminderLog();
                            r.ApproverListId = Convert.ToInt32(sqlDataReader["TripApproverListId"]);
                            r.TripId = Convert.ToInt32(sqlDataReader["TripId"]);
                            r.EmployeeId = Convert.ToInt32(sqlDataReader["ApproverId"]);
                            r.ReminderMessage = sqlDataReader["RemiderMessage"].ToString();
                            r.LastSentReminderLevel = Convert.ToInt32(sqlDataReader["LastSentReminderLevel"]);
                            r.MinutesAfterLastReminder = Convert.ToInt32(sqlDataReader["MinutesAfterLastReminder"]);
                            r.MinutesSinceLastSent = Convert.ToInt32(sqlDataReader["MinutesTillLastSent"]);
                            r.NextReminderLevel = Convert.ToInt32(sqlDataReader["NextReminderLevel"]);
                            if (!string.IsNullOrEmpty(r.ReminderMessage))
                            {
                                BasicLogs.Add(r);
                            }
                        }
                    }
                }
            }
            return BasicLogs;
        }           
        public static int InsertReminderLog(ReminderLog log)
        {
            string query = string.Format("insert into Company.ReminderLog values({0}, {1}, {2}, getdate(), {3}, {4})",log.ClaimId,log.TripId,log.EmployeeId,log.NextReminderLevel,log.ApproverListId);
            var conString = ConfigurationManager.ConnectionStrings["TraveoDBSQLConnection"].ToString();
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return 1;
        }

    }
}
