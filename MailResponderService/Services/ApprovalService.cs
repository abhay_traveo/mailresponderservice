﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Configuration;
namespace MailResponderService.Services
{
    class ApprovalService
    {
        public dynamic SendTripApproval(EmailApproverRequest trip)
        {
            var url = ConfigurationManager.AppSettings["TripApi"];
            var client = new RestClient(url);

            var request = new RestRequest("ApproveOrReturnTripByEmail", Method.POST);
            var requestBody = trip;
            request.AddParameter("application/json", JsonConvert.SerializeObject(requestBody),
                ParameterType.RequestBody);

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            Console.WriteLine("trip approver service response received with content-" + content);
            return content;
        }
        public dynamic SendClaimApproval(EmailApproverRequest claim)
        {
            var url = ConfigurationManager.AppSettings["ClaimApi"];
            var client = new RestClient(url);

            var request = new RestRequest("ApproveOrReturnClaimByEmail", Method.POST);
            var requestBody = claim;
            request.AddParameter("application/json", JsonConvert.SerializeObject(requestBody),
                ParameterType.RequestBody);

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            Console.WriteLine("claim approver service response received with content-" + content);

            return content;
        }

        public string GetTripSummary(int tripId)
        {
            var url = ConfigurationManager.AppSettings["TraveoApiUrl"];
            url += "api/Email/";
            var client = new RestClient(url);

            var request = new RestRequest("GetTripSummaryForEmailResponder", Method.GET);
            request.AddParameter("tripId", tripId);

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return content;
        }

    }
    public class EmailApproverRequest
    {
        public int ApproverId { get; set; }
        public int ApproverorReturnedType { get; set; }
        public int TripOrClaimId { get; set; }
        public string AuthToken { get; set; }
        public string Message { get; set; }
    }
}
