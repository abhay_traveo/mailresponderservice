﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripCreationWebjob.Models
{
    public class CreateTripRequest
    {
        public dynamic TripObj { get; set; }
        public int CompanyId { get; set; }
        public int EmployeeId { get; set; }
        public int LoginId { get; set; }
        public string AuthenticationToken { get; set; }
    }
}
