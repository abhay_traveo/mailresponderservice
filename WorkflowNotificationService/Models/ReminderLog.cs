﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowNotificationService.Models
{
    public class ReminderLog
    {
        public int ClaimId { get; set; }
        public int TripId { get; set; }
        public int ApproverListId { get; set; }
        public int MinutesSinceLastSent { get; set; }
        public int MinutesAfterLastReminder { get; set; }
        public int EmployeeId { get; set; }
        public int LastSentReminderLevel { get; set; }
        public int NextReminderLevel { get; set; }
        public string ReminderMessage { get; set; }
    }
}
