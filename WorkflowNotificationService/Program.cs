﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using WorkflowNotificationService.Activities;
using WorkflowNotificationService.Models;

namespace WorkflowNotificationService
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            List<ReminderLog> basicTripLogs = SqlHelper.getBasicTripReminderLogs();
            List<ReminderLog> basicClaimLogs = SqlHelper.getBasicClaimReminderLogs();
            var groupByClaim = basicClaimLogs.GroupBy(a => a.ClaimId).Select(grp => grp.ToList()).ToList();
            var groupByTrip = basicTripLogs.GroupBy(a => a.TripId).Select(grp => grp.ToList()).ToList();
            List<ReminderLog> NotificationsToBeSent = new List<ReminderLog>();
            foreach(var logs in groupByClaim)
            {
                List<ReminderLog>presentLog = logs.OrderByDescending(a => a.NextReminderLevel).ToList();
                NotificationsToBeSent.Add(presentLog[0]);
            }
            NotificationsToBeSent = NotificationsToBeSent.Where(a => a.MinutesSinceLastSent >= a.MinutesAfterLastReminder).ToList();
            int claimnotifications = NotificationsToBeSent.Count;
            Console.WriteLine("Number of claim Notifications to be sent are " + NotificationsToBeSent.Count);
            foreach (var logs in groupByTrip)
            {
                List<ReminderLog> presentLog = logs.OrderByDescending(a => a.NextReminderLevel).ToList();
                NotificationsToBeSent.Add(presentLog[0]);
            }
            NotificationsToBeSent = NotificationsToBeSent.Where(a => a.MinutesSinceLastSent >= a.MinutesAfterLastReminder).ToList();
            Console.WriteLine("Number of trip Notifications to be sent are " + (NotificationsToBeSent.Count - claimnotifications));
            foreach(var notification in NotificationsToBeSent)
            {
                if (!string.IsNullOrEmpty(notification.ReminderMessage))
                {
                    if (notification.MinutesSinceLastSent >= notification.MinutesAfterLastReminder)
                    {
                        NotificationService notificationService = new NotificationService();
                        Console.WriteLine("trying to send notification for claimId:" + notification.ClaimId + " or tripId:" + notification.TripId+" to the employeeId:"+notification.EmployeeId);
                        dynamic response = JsonConvert.DeserializeObject(notificationService.SendNotification(notification.TripId, notification.ClaimId, notification.ReminderMessage));
                        if (response != null && response.content == "Success")
                        {
                            SqlHelper.InsertReminderLog(notification);
                            Console.WriteLine("Notificatio sent Successfully");
                        }
                        else
                        {
                            Console.WriteLine("Could not send notification as notification service returned this ");
                            Console.WriteLine(response.ToString());
                        }
                    }
                }
            }
        }
    }
}
