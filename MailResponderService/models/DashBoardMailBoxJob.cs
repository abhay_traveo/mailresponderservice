﻿using OpenPop.Mime;
using System.Collections.Generic;

namespace MailResponderService.models
{
    public class DashBoardMailBoxJob
    {
        //public MailBoxJob MailBoxJobModel { get; set; }
        public Dictionary<int, Message> messages = new Dictionary<int, Message>();
        public string mess { get; set; }
        public string data { get; set; }
        public List<MailMessege> Inbox { get; set; }
    }
}
