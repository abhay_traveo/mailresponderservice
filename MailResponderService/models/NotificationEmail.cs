﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailResponderService.models
{
    public class NotificationEmail
    {
        public string approverEmail { get; set; }
        public int employeeId { get; set; }
        public int companyId { get; set; }
        public int doneById { get; set; }
        public string approverName { get; set; }
        public string content { get; set; }
        public string subject { get; set; }
        public string companyName { get; set; }
        public int tripType { get; set; }
    }
}
