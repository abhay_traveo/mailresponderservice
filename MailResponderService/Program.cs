﻿using System;
using AE.Net.Mail;
using System.Configuration;
using OpenPop.Common.Logging;
using MailResponderService.models;
using MailResponderService.Activities;
using MailResponderService.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace MailResponderService
{
    class Program
    {
        private const string smtpHost = "smtp.gmail.com";
        private static string smtpUser = ConfigurationManager.AppSettings["EmailId"];
        private static string smtpPassword = ConfigurationManager.AppSettings["Password"];
        static void Main(string[] args)
        {
            DashBoardMailBoxJob model = new DashBoardMailBoxJob();
            model.Inbox = new List<MailMessege>();
            var traveoUrl = ConfigurationManager.AppSettings["TraveoUrl"];
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            char[] separators = { '\r', '\n', '\\', '/', '<', '>', ':', '-' };
            try
            {
                EmailConfiguration email = new EmailConfiguration();
                email.POPServer = "imap.gmail.com"; // type your popserver
                email.POPUsername = ConfigurationManager.AppSettings["EmailId"]; // type your username credential
                email.POPpassword = ConfigurationManager.AppSettings["Password"]; // type your password credential
                email.IncomingPort = "993";
                email.IsPOPssl = true;
                ImapClient ic = new ImapClient(email.POPServer, email.POPUsername, email.POPpassword, AuthMethods.Login, Convert.ToInt32(email.IncomingPort), (bool)email.IsPOPssl);
                // Select a mailbox. Case-insensitive
                ic.SelectMailbox("INBOX");

                var mailMessage = ic.SearchMessages(SearchCondition.Unseen(), false, true).ToList();
                var mm = mailMessage.Select(a => a.Value).ToList();
                // Note that you must specify that headersonly = false
                //// when using GetMesssages().
                //MailMessage[] mm = ic.GetMessages(start, end, false);
                //mm = mm.Where(m => !m.Flags.HasFlag(Flags.Seen)
                ////                                     && !m.Flags.HasFlag(Flags.Deleted)).ToArray();
                mm = mm.Where(m => m.Subject.Contains("approval requested by")).ToList();
                Console.WriteLine("got " + mm.Count + " emails to respond for "+ ConfigurationManager.AppSettings["EmailId"]);
                foreach (var item in mm)
                {
                    MailMessege obj = new MailMessege();
                    try
                    {
                        string senderId = item.From.Address;
                        int tripId = 0;
                        int claimId = 0;
                        Console.WriteLine("responding to email from-" + senderId);
                        int approverId = SqlHelper.GetIdByEmail(senderId);
                        Console.WriteLine("approver id for " + senderId + "-" + approverId);
                        if (approverId > 0)
                        {
                            string body = item.Body.ToString();
                            string response = body.Split(separators)[0];
                            string[] wrote = {  "wrote:"};
                            string originalMaik = body.Split(wrote,StringSplitOptions.None).Last();
                            string subject = item.Subject.ToString();
                            int approveOrReject = 2;
                            response = response.ToUpper();
                            response = response.Trim();
                            Console.WriteLine("response received is -" + response);
                            if (response == "YES" || response == "APPROVE" || response == "APPROVED")
                            {
                                approveOrReject = 1;
                            }
                            else if (response == "NO" || response == "REJECT" || response == "REJECTED")
                            {
                                approveOrReject = 0;
                            }
                            string message = body.Split(separators)[1];

                            string subString = "";
                            //if (body.Contains(traveoUrl))
                            //{
                            //    var index = body.IndexOf(traveoUrl);
                            //    subString = body.Substring(index);
                            //    Console.WriteLine("url-" + subString);
                            //}
                            //else
                            //{
                            //    Console.WriteLine("traveo url was not found in the body -"+traveoUrl);
                            //}
                            if (subject.Contains("TripId"))
                            {
                                string[] tripdelimiter = { "TripId-" };
                                var trippart = subject.Split(tripdelimiter,StringSplitOptions.None)[1];
                                var trip = trippart.Split(separators)[1];
                                int.TryParse(trip, out tripId);
                                Console.WriteLine("got tripId-" + trip);
                            }
                            else if (subject.Contains("ClaimId"))
                            {
                                string[] claimdelimiter = { "ClaimId-" };
                                var claimpart = subject.Split(claimdelimiter, StringSplitOptions.None)[1];

                                if (claimpart.Contains("T-"))
                                {
                                    var trip = claimpart.Split(separators)[1];
                                    claimId = SqlHelper.GetClaimIdByTripId(trip);
                                }
                                else
                                {
                                    var claim = claimpart.Split(separators)[0];
                                    int.TryParse(claim, out claimId);
                                }

                                Console.WriteLine("got claim Id-" + claimId);
                            }
                            if (SqlHelper.IsEmailApprovalAllowed(claimId, tripId))
                            {
                                if (approveOrReject < 2)
                                {
                                    if (tripId != 0)
                                    {

                                        if (approverId == SqlHelper.getLastApproverByTripId(tripId))
                                        {
                                            EmailApproverRequest t = new EmailApproverRequest();
                                            t.ApproverId = approverId;
                                            t.ApproverorReturnedType = approveOrReject;
                                            t.TripOrClaimId = tripId;
                                            ApprovalService ap = new ApprovalService();
                                            Console.WriteLine("trying to approve/reject(" + t.ApproverorReturnedType + ") tripid-" + tripId + " with approverId-" + approverId);
                                            ap.SendTripApproval(t);
                                            // item.Flags = Flags.Seen;
                                        }
                                        else
                                        {
                                            Console.WriteLine("responder is not correct approver");
                                        }
                                    }

                                    else if (claimId != 0)
                                    {

                                        if (approverId == SqlHelper.getLastApproverByClaimId(claimId))
                                        {
                                            EmailApproverRequest t = new EmailApproverRequest();
                                            t.ApproverId = approverId;
                                            t.ApproverorReturnedType = approveOrReject;
                                            t.TripOrClaimId = claimId;
                                            ApprovalService ap = new ApprovalService();

                                            Console.WriteLine("trying to approve/reject(" + t.ApproverorReturnedType + ") claimId-" + claimId + " with approverId-" + approverId);
                                            ap.SendClaimApproval(t);
                                            //item.Flags = Flags.Seen;
                                        }
                                        else
                                        {
                                            Console.WriteLine("responder is not correct approver");
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("response was incorrect hence sending mail to correct response");
                                    if (tripId != 0)
                                    {
                                        var emailcontents = SqlHelper.GetNotificationByTripId(tripId);
                                        ApprovalService ap = new ApprovalService();
                                        string tripSummary = ap.GetTripSummary(tripId);

                                        SendReplies(CreateReply(item, ConfigurationManager.AppSettings["EmailId"], traveoUrl + "#/ApproveTrip/" + tripId, emailcontents, response, tripSummary));
                                    }
                                    else if (claimId != 0)
                                    {
                                        var emailcontents = SqlHelper.GetNotificationByClaimId(claimId);
                                        SendReplies(CreateReply(item, ConfigurationManager.AppSettings["EmailId"], traveoUrl + "#/ViewClaim/" + claimId + "/Approve", emailcontents, response, ""));
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("not Performing Any action as email approval is not allowed by company");
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        DefaultLogger.Log.LogError(
                            "TestForm: Message fetching failed: " + e.Message + "\r\n" +
                            "Stack trace:\r\n" +
                            e.StackTrace);
                    }

                }
                ic.Dispose();

            }

            catch (Exception e)
            {
                model.mess = "Error occurred retrieving mail. " + e.Message;
            }
            finally
            {

            }
        }
        private static System.Net.Mail.MailMessage CreateReply(AE.Net.Mail.MailMessage source, string imapUser, string Url, NotificationEmail contents, string response, string summary)
        {
            System.Net.Mail.MailMessage reply = new System.Net.Mail.MailMessage(new MailAddress(imapUser, "Traveo"), source.From);

            // Get message id and add 'In-Reply-To' header
            string id = source.MessageID;
            reply.Headers.Add("In-Reply-To", id);

            // Try to get 'References' header from the source and add it to the reply
            var references = source.Headers["References"].ToString();

            if (!string.IsNullOrEmpty(references))
                references += ' ';

            reply.Headers.Add("References", references + id);

            // Add subject
            if (!source.Subject.StartsWith("Re:", StringComparison.OrdinalIgnoreCase))
                reply.Subject = "Re: ";

            reply.Subject += source.Subject;

            // Add body
            StringBuilder text = new StringBuilder();

            text.Append("<div><p>Provided input is  incorrect</p><p>Please reply with (Yes/No)</p></div><br/>" +
                "<p>Your response was :" + response + "</p><br/>");
            //text.AppendFormat("<div>On {0}, ", source.Date.ToString("ddd, MMM d,yyyy"));
            //text.AppendFormat(" at {0}", source.Date.ToString("hh:mm tt,"));

            //if (source.From.DisplayName != null)
            //    text.Append(source.From.DisplayName + ' ');

            //text.AppendFormat("<<a href=\"mailto:{0}\">{0}</a>> wrote:<br/>", source.From.Address);

            //text.Append("<p>Original Message was</p><br/>");
            // Append original text body if it exists
            if (source.Body != null)
            {
                text.Append("<blockquote style=\"margin: 0 0 0 5px;");
                text.Append(" border-left: 2px blue solid;");
                text.Append(" padding-left: 5px\">");
                if (string.IsNullOrEmpty(summary))
                {
                    text.Append("<p>Dear " + contents.approverName + ",</p><p>" + contents.content + 
                        "traveo to take action on this request</p><p>Regards</p><br><p>Travel Management(" + contents.companyName + ")</p>");

                }
                else
                {
                    text.Append("<p>Dear " + contents.approverName + ",</p><p>" + contents.content +
                       "  to take action on this request</p>" + summary);
                }
                text.Append("</blockquote><br/></div>");
            }
            //            reply.Body = "<p>Provided input is  incorrect</p><p>Please reply with (Yes/No) or <br><br><a href=\"" + Url + "\">Click here</a></p>";

            reply.Body = text.ToString();
            reply.IsBodyHtml = true;

            return reply;
        }
        private static void SendReplies(System.Net.Mail.MailMessage msg)
        {
            using (SmtpClient client = new SmtpClient(smtpHost, 587))
            {
                // Set SMTP client properties
                string id = msg.Headers["In-Reply-To"];
                client.EnableSsl = true;
                string references = msg.Headers["References"];

                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(smtpUser, smtpPassword);

                // Send
                bool retry = true;

                try
                {
                    client.Send(msg);
                    retry = true;
                }
                catch (Exception ex)
                {
                    if (!retry)
                    {
                        Console.WriteLine("Failed to send email reply to " + msg.To.ToString() + '.');
                        Console.WriteLine("Exception: " + ex.Message);
                        return;
                    }

                    retry = false;
                }
                finally
                {
                    msg.Dispose();
                }


            }
        }
    }
}
